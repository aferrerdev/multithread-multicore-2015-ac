#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <strings.h>
#include <assert.h>

#define N (4000L)
#define ND (N*N/100)

int A[N][N],B[N][N],C[N][N],CD[N][N];
struct {
    int i,j,v;
}AD[ND];

long long Suma=0;

int main()
{
    int i,j,k,neleC=0;

    double total_t_1;
    clock_t start_t_bucle1, end_t_bucle1;

    start_t_bucle1 = clock();
    bzero(A,sizeof(int)*(N*N));
    bzero(C,sizeof(int)*(N*N));
    bzero(CD,sizeof(int)*(N*N));
    
    for (i=0;i<N;i++)
        for (j=0;j<N;j++)
            B[i][j] = rand()%1000;
    
    for(k=0;k<ND;k++)
    {
        AD[k].i=rand()%(N-1);
        AD[k].j=rand()%(N-1);
        AD[k].v=rand()%100;
        while (A[AD[k].i][AD[k].j]) {
            if(AD[k].i < AD[k].j)
                AD[k].i = (AD[k].i + 1)%N;
            else 
                AD[k].j = (AD[k].j + 1)%N;
        }
        A[AD[k].i][AD[k].j] = AD[k].v;
    }
    end_t_bucle1 = clock();

    // Segons que ha trigat:
    total_t_1 = ((double)(end_t_bucle1 - start_t_bucle1)) / CLOCKS_PER_SEC;
    printf("Bucles Inicialitzacio => start: %d end:%d diferencia:%d \n", start_t_bucle1, end_t_bucle1, end_t_bucle1 - start_t_bucle1);
    printf("Bucles Inicialitzacio => Total time taken by CPU: %f\n", total_t_1);

    ////Matriu x matriu original (recorregut de C per columnes)
    //for (i=0;i<N;i++)
    //    for (j=0;j<N;j++)
    //        for (k=0;k<N;k++)
    //            C[j][i] += A[j][k] * B[k][i];
 
    //Matriu dispersa per matriu
    // PARAL.LELITZAR !!!
      // Clock
    start_t_bucle1 = clock();
    for(i=0;i<N;i++)
        for (k=0;k<ND;k++)
            CD[AD[k].i][i] += AD[k].v * B[AD[k].j][i];

    end_t_bucle1 = clock();

    // Segons que ha trigat:
    total_t_1 = ((double)(end_t_bucle1 - start_t_bucle1))/ CLOCKS_PER_SEC;
    printf("Bucle Par.Obligatoria => start: %d end:%d diferencia:%d \n", start_t_bucle1, end_t_bucle1, end_t_bucle1 - start_t_bucle1);
    printf("Bucle Par.Obligatoria => Total time taken by CPU: %f\n", total_t_1);
            
    // Comprovacio MD x M -> M i MD x MD -> M
    // PARAL.LELITZACIO OPCIONAL
    start_t_bucle1 = clock();
    Suma =  neleC = 0;
    for (i=0;i<N;i++)
        for(j=0;j<N;j++)
         {
             Suma += CD[i][j];
             if (CD[i][j]) neleC++;
             //if (C[i][j] != CD[i][j])
             //   printf("Diferencies C i CD pos %d,%d: %d != %d\n",i,j,C[i][j],CD[i][j]);
         }
    end_t_bucle1 = clock();

    // Segons que ha trigat:
    total_t_1 = ((double)(end_t_bucle1 - start_t_bucle1)) / CLOCKS_PER_SEC;
    printf("Bucles Par.Opcional => start: %d end:%d diferencia:%d \n", start_t_bucle1, end_t_bucle1, end_t_bucle1 - start_t_bucle1);
    printf("Bucles Par.Opcional => Total time taken by CPU: %f\n", total_t_1);
            
            

    printf ("\nNumero elements de la matriu dispersa C %d\n",neleC);
    printf("Suma dels elements de C %lld \n",Suma);
}
