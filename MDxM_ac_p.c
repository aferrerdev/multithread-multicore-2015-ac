#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <strings.h>
#include <assert.h>
#include <pthread.h>

#define N (2000L)
#define ND (N*N/100)
#define NUM_THREADS 64

int A[N][N],B[N][N],C[N][N],CD[N][N];

struct {
    int i,j,v;
}AD[ND];

// Struct per guardar la informacio dels threads
typedef struct {
	int id_thread;
	int porcion;
	int inici;
	int max_bucle;
}info_thread;

long long Suma[NUM_THREADS];
int neleC[NUM_THREADS];
long long SumaTotal = 0;
int neleCTotal = 0;
int resto;

// ==== Implementacio funcions threads ====
pthread_t tid[NUM_THREADS];

// Funcio per a calcular els index inicial i maxim dels bucles a paralelitzar.
info_thread * calcular_indexos(void* arg)
{
	int i, max_bucle;
	info_thread * info = (info_thread*) arg;
	if(info->id_thread < resto)
	{
		i = (info->id_thread) * (info->porcion + 1);
		max_bucle = (info->id_thread + 1) * (info->porcion + 1);
	}
	else
	{
		i = (info->id_thread) * (info->porcion);
		if((info->id_thread + 1) == NUM_THREADS)
			max_bucle = N;
		else
			max_bucle = (info->id_thread + 1) * (info->porcion);
	}
	info->max_bucle = max_bucle;
	info->inici = i;

	return info;
}
// Paralelitzacio Obligatoria.
void * codi_thread(void *arg)
{
	int k, i, max_bucle;;
	info_thread * info = calcular_indexos(arg);
	i = info->inici;
	max_bucle = info->max_bucle;
	// Iniciar bucles:
	for(i;i<max_bucle;i++)
        for (k=0;k<ND;k++)
            CD[AD[k].i][i] += AD[k].v * B[AD[k].j][i];

	return 0;
}
// Codi Thread Opcional.
void * opcional_thread(void *arg)
{
	int k, j, i, max_bucle;
	info_thread * info = calcular_indexos(arg);
    i = info->inici;
	max_bucle = info->max_bucle;
	// Iniciar bucles.
	int pos = (info->id_thread);
    Suma[pos] =  neleC[pos] = 0;
    for (i;i<max_bucle;i++)
    {
        for(j=0;j<N;j++)
         {
         	 Suma[pos] += CD[i][j];
             if (CD[i][j]) neleC[pos]++;
             //if (C[i][j] != CD[i][j])
             //   printf("Diferencies C i CD pos %d,%d: %d != %d\n",i,j,C[i][j],CD[i][j]);
         }
    }
	return 0;
}

// ======= Main ======================
int main()
{
    int i,j,k, n;

    // Calcular residu per si ens dona decimals:
    int resto = N % NUM_THREADS;

    // Vector de informacio dels threads
    info_thread info[NUM_THREADS];
    
    bzero(A,sizeof(int)*(N*N));
    bzero(C,sizeof(int)*(N*N));
    bzero(CD,sizeof(int)*(N*N));
    
    for (i=0;i<N;i++)
        for (j=0;j<N;j++)
            B[i][j] = rand()%1000;
    
    for(k=0;k<ND;k++)
    {
        AD[k].i=rand()%(N-1);
        AD[k].j=rand()%(N-1);
        AD[k].v=rand()%100;
        while (A[AD[k].i][AD[k].j]) {
            if(AD[k].i < AD[k].j)
                AD[k].i = (AD[k].i + 1)%N;
            else 
                AD[k].j = (AD[k].j + 1)%N;
        }
        A[AD[k].i][AD[k].j] = AD[k].v;
    }

    ////Matriu x matriu original (recorregut de C per columnes)
    //for (i=0;i<N;i++)
    //    for (j=0;j<N;j++)
    //        for (k=0;k<N;k++)
    //            C[j][i] += A[j][k] * B[k][i];
 
    //Matriu dispersa per matriu
    // PARAL.LELITZAR !!!
    /*
    for(i=0;i<N;i++)
        for (k=0;k<ND;k++)
            CD[AD[k].i][i] += AD[k].v * B[AD[k].j][i];
    */
    printf("\n Paral·lelitzacio Matriu dispersa per matriu: \n");
    // Bucle crear N threads
    for(n = 0; n < NUM_THREADS; n++)
    {
    	// Calcular porcio
		info[n].porcion = N/NUM_THREADS;
    	info[n].id_thread = n;
    	if(pthread_create(&tid[n],NULL,codi_thread,&info[n])==0){
    		//printf("Thread %d creat. \n",n);
    	}
    }
    printf("El %d threads s'han creat \n",n);
    // Bucle de espera de finalizacion de los threads
    for(n = 0; n < NUM_THREADS; n++)
    {
    	pthread_join(tid[n],NULL);
    	//printf("El Thread %d ha acabat. \n",n);
    }
    printf("El %d threads han acabat \n",n);    
    // Comprovacio MD x M -> M i MD x MD -> M
    // PARAL.LELITZACIO OPCIONAL
   	/*Suma[0] =  neleC[0] = 0;
    for (i=0;i<N;i++)
    {
        for(j=0;j<N;j++)
         {
             Suma[0] += CD[i][j];
             if (CD[i][+j]) neleC[0]++;
             //if (C[i][j] != CD[i][j])
             //   printf("Diferencies C i CD pos %d,%d: %d != %d\n",i,j,C[i][j],CD[i][j]);
         }
    }
    neleCTotal+=neleC[0];
    SumaTotal+=Suma[0];
    */
    printf("\n Paral·lelitzacio opcional: \n");
    for(n = 0; n < NUM_THREADS; n++)
    {
    	// Calcular porcio
    	info[n].porcion = N/NUM_THREADS;
    	info[n].id_thread = n;
    	if(pthread_create(&tid[n],NULL,opcional_thread,&info[n])==0){
    		printf("Thread opcional %d creat. \n",n);
    	}
    }
    printf("El %d threads opcionals s'han creat \n",n);
    // Bucle de espera de finalizacion de los threads
    for(n = 0; n < NUM_THREADS; n++)
    {
    	pthread_join(tid[n],NULL);
    	printf("El Thread opcional %d ha acabat. \n",n);
    }
    for(n = 0; n < NUM_THREADS; n++){
        neleCTotal+=neleC[n];
        SumaTotal+=Suma[n];

    }
    printf ("\nNumero elements de la matriu dispersa C %d\n",neleCTotal);
    printf("Suma dels elements de C %lld \n",SumaTotal);

    exit(0);
}
